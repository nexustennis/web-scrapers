var http = require('http');
var https = require('https');

function request(url, callback) {
	if (url.substr(0, 5) == "https") {
		https.get(url, function(res) {
			var body = '';

		    res.on('data', function(chunk) {
		        body += chunk;
		    });

		    res.on('end', function() {
		        callback(null, body);
		    });
		}).on("error", function(err) {
			callback(err, null);
		});
	} else if (url.substr(0, 4) == "http") {
		http.get(url, function(res) {
			var body = '';

		    res.on('data', function(chunk) {
		        body += chunk;
		    });

		    res.on('end', function() {
		        callback(null, body);
		    });
		}).on("error", function(err) {
			callback(err, null);
		});
	}
	
}

module.exports = request;