var conn = require("../database");

function getAllATPplayers(callback) {
	query = "SELECT MIN(rankingID) AS rankingID, playerName, profileURL FROM rankings GROUP BY playerName, profileURL";
	conn.query(query, getAllplayersResult);

	function getAllplayersResult(err, rows, fields) {
		if (err) throw err;
		callback(rows);
	}
}

module.exports = getAllATPplayers;