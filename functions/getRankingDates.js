var conn = require("../database");

function getRankingDates(organisation, category, callback) {
	var query = "SELECT * FROM rankingDates WHERE organisation = ? AND category = ? ORDER BY date DESC";
	conn.query(query, [organisation, category], getRankingDatesResult);

	function getRankingDatesResult(err, rows, fields) {
		callback(rows);
	}
}

module.exports = getRankingDates;