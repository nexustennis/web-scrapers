var request         = require("../request");
var cheerio         = require('cheerio');
var conn            = require("../database");

function getNewATPsinglesRankingDates(callback) {

	request("http://www.atpworldtour.com/en/rankings/singles", processHTML);

	function processHTML(err, body) {
		$ = cheerio.load(body);
		$('#filterHolder').filter(extractData);

		function extractData() {
			var data = $(this);
			var weeks = data.children()[0].children[1].children[1].children[1].children[3].children;
			
			var dates = [];

			for (var i = 3; i < weeks.length; i += 2) {
				var date = [];

				date.push("atp");
				date.push("singles");
				date.push(weeks[i].attribs["data-value"]);

				dates.push(date);
			}

			var newDates = [];

			getExistingRankingDates("atp", "singles", existingRankingDatesResult);

			function existingRankingDatesResult(existingDates) {
				for (var i = 0; i < dates.length; i++) {
					if (!dateExists(dates[i][2])) newDates.push(dates[i]);
				}

				callback(newDates);

				function dateExists(date) {
					for (var i = 0; i < existingDates.length; i++) {
						var day          = existingDates[i].date.getDate();
						if (day < 10) day = "0" + day.toString();
						var month        = existingDates[i].date.getMonth() + 1;
						if (month < 10) month = "0" + month.toString();
						var year         = existingDates[i].date.getFullYear();
						var existingDate = year + "-" + month + "-" + day;

						if (date == existingDate) return true;
					}

					return false;
				}
			}
		}
	}
}

function getExistingRankingDates(organisation, category, callback) {
	var query = "SELECT date FROM rankingDates WHERE organisation = ? AND category = ? ORDER BY date DESC";
	conn.query(query, [organisation, category], getRankingDatesResult);

	function getRankingDatesResult(err, rows, fields) {
		callback(rows);
	}
}

function insertRankingDates(organisation, category, dates, callback) {
	console.log(dates);
	var query = "INSERT INTO rankingDates (organisation, category, date) VALUES ?";
	conn.query(query, [dates], rankingDatesResult);

	function rankingDatesResult(err, fields) {
		if (err) throw err;
		console.log(fields);
		var rankingDateID = fields.insertId;

		for (var i = 0; i < dates.length; i++) {
			dates[i].push(rankingDateID);
			rankingDateID++;
		}

		console.log(dates);

		callback();
	}
}

module.exports = {
	getExistingRankingDates,
	insertRankingDates,
	getNewATPsinglesRankingDates
}