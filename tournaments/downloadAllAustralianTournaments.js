var cheerio = require('cheerio');
var request = require("../functions/request");
var fs      = require("fs");

getAustralianTournaments();

function getAustralianTournaments() {
	var tournamentGroups= [];
	var tournaments = [];
	var otherTournaments = [];
	var i = 1;

	console.log("Getting Number of Pages\n");

	getNumberOfPages();

	function getNumberOfPages() {
	
		request("https://tournaments.tennis.com.au/home.aspx?tlid=1&p=1", processHTML);

		function processHTML(err, body) {
			$ = cheerio.load(body);
			$('.pagenrs').filter(extractData);

			function extractData() {
				var data = $(this);
				var pageNumbersList = data.children();

				for (i = 0; i < pageNumbersList.length; i++) {
					var data = pageNumbersList[i].children[0].data;
					if (!isNaN(data)) numberOfPages = data;
				}

				i = 1;
				console.log("Number of Pages: " + numberOfPages + "\n");
				console.log("Getting Tournaments\n");

				getTournaments();
			}
		}
	}	

	function getTournaments() {
		if (i > numberOfPages) {
			i = 0;
			console.log("Getting Main Tournament Data\n");
			tournamentGroups.splice(0, 2)
			getMainDetails();
			return;
		}

		console.log("Page " + i + "\n");

		var url = "https://tournaments.tennis.com.au/home.aspx?tlid=1&p=" + i;
		console.log(url);
		
		request(url, processTournaments);

		function processTournaments(err, body) {
			$ = cheerio.load(body);
			$('.middle').filter(extractTournamentData);

			function extractTournamentData() {
				var data = $(this);
				var temp = data.children()[2].children[2].children;
				var tournamentsHTML = temp.slice(1, temp.length - 1);

				for (var j = 0; j < tournamentsHTML.length; j++) {
					var tournament = [];

					var startDay   = tournamentsHTML[j].children[1].children[0].data.split(" ")[0].split("/")[0];
					var startMonth = tournamentsHTML[j].children[1].children[0].data.split(" ")[0].split("/")[1];
					var startYear  = "2019";

					var endDay     = tournamentsHTML[j].children[1].children[0].data.split(" ")[2].split("/")[0];
					var endMonth   = tournamentsHTML[j].children[1].children[0].data.split(" ")[2].split("/")[1];
					var endYear    = "2019";

					var startDate  = startYear + "-" + startMonth + "-" + startDay;
					var endDate    = endYear   + "-" + endMonth   + "-" + endDay;

					var tournamentURL  = tournamentsHTML[j].children[2].children[0].attribs.href;
					var tournamentName = tournamentsHTML[j].children[2].children[0].children[0].data;

					tournament.push(tournamentName);
					tournament.push(tournamentURL);
					tournament.push(startDate);
					tournament.push(endDate);

					tournamentGroups.push(tournament);
				}

				i++;
				getTournaments();
			}
		}
	}

	function getMainDetails() {
		if (i == tournamentGroups.length) {
			i = 0;
			console.log("Getting Factsheet Data");
			getFactsheetData();
			return;
		}

		var url = "https://tournaments.tennis.com.au/" + tournamentGroups[i][1];
		console.log(url);

		request(url, processHTML);

		function processHTML(err, body) {
			$ = cheerio.load(body);
			$('#base_container_ctl01_content_ctl02_cphPage_cphPage_tblContactInformation').filter(extractData);

			function extractData() {
				var data = $(this);

				try {
					var addressHTML = data.children()[0].children[1].children[2].children[0].children[0].children[0].children[0].children;
				} catch (e) {
					var addressHTML = data.children()[0].children[0].children[2].children[0].children[0].children[0].children[0].children;
				}

				var address = "";

				address += addressHTML[0].data;

				for (var j = 2; j < addressHTML.length; j += 2) {
					address += " " + addressHTML[j].data;
				}

				var components = address.split(" ");
				url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBL-SId6Ibn2HTOgMsihSbD-47Fk4GmziI&address=";

				url += components[0];

				for (var j = 1; j < components.length; j++) {
					if (components[j] != "&") url += "+" + components[j];
				}

				request(url, getCoordinates);
			}
		}

		function getCoordinates(err, data) {
			try {
				var coordinates = JSON.parse(data).results[0].geometry.location;
				var address = JSON.parse(data).results[0].formatted_address;

				var latitude  = coordinates.lat;
				var longitude = coordinates.lng;
			} catch(e) {
				var address   = null;
				var latitude  = null;
				var longitude = null;
			}

			tournamentGroups[i].push(address);
			tournamentGroups[i].push(latitude);
			tournamentGroups[i].push(longitude);

			i++;
			getMainDetails();
		}
	}

	function getFactsheetData() {
		if (i == tournamentGroups.length) {
			exportOtherTournaments();
			return;
		}

		var url = "https://tournaments.tennis.com.au/sport/factsheet.aspx?id=" + tournamentGroups[i][1].substr(25, tournamentGroups[i][1].length);
		console.log(url);

		request(url, processHTML);

		function processHTML(err, body) {
			$ = cheerio.load(body);
			$('#organizationmenu').filter(extractData);

			function extractData() {
				try {
					var data = $(this);
					var components = data.parent().children();
					
					var events = [];

					var options = [
						"Australian Money Tournament: Single",
						"Australian Money Tournament: Double",
						"12 & Under: Singles",
						"12 & Under: Doubles",
						"14 & Under: Singles",
						"14 & Under: Doubles",
						"16 & Under: Singles",
						"16 & Under: Doubles",
						"18 & Under: Singles",
						"18 & Under: Doubles",
					];

					console.log(tournamentGroups[i][0]);

					for (var j = 5; true; j++) {
						try {
							var title  = components[j].children[1].children[0].data.replace(/(\n|\t)/gm, ""); //error here!!!!!!!!!!!!!!!1
							if (!options.includes(title)) continue;
							var format = title.split(": ")[1];
							var event  = title.split(": ")[0];
							
							for (var k = 1; k < components[j].children[2].children.length - 1; k++) {
								if (components[j].children[2].children[k].children[0].children[0].data == "Grading:") {
									if (event.includes("Australian Money Tournament")) {
										var temp = components[j].children[2].children[k].children[1].children[0].data.split(" (");
										level = temp[0];
										var totalFinancialCommitment = temp[1].substr(0, temp[1].length - 1);
									} else if (event.includes("& Under")) {
										var level = components[j].children[2].children[k].children[1].children[0].data;
										if (level == "Platinum Series") level = "platinum";
										else if (level == "Gold Series") level = "gold";
										else if (level == "Silver Series") level = "silver";
										else if (level == "Bronze Series") level = "bronze";
										else level = null;
										var totalFinancialCommitment = "$0";
									}
								}
							}
							
							var singlesDrawSizeMen   = null;
							var doublesDrawSizeMen   = null;
							var singlesDrawSizeWomen = null;
							var doublesDrawSizeWomen = null;

							var draws = components[j].children[2].children[components[j].children[2].children.length - 2].children[1].children[2].children;
							
							for (var k = 0; k < draws.length; k++) {
								if (draws[k].name == "tr") {
									var drawName = draws[k].children[1].children[0].data;

									if (drawName == "Mens Main Draw") {
										if      (format.includes("Single")) singlesDrawSizeMen   = draws[k].children[2].children[0].data;
										else if (format.includes("Double")) doublesDrawSizeMen   = draws[k].children[2].children[0].data;
									} else if (drawName == "Womens Main Draw") {
										if      (format.includes("Single")) singlesDrawSizeWomen = draws[k].children[2].children[0].data;
										else if (format.includes("Double")) doublesDrawSizeWomen = draws[k].children[2].children[0].data;
									}
								}
							}

							if      (event.includes("Australian Money Tournament")) var ageGroup = "open";
							else if (event.includes("10 & Under"))                  var ageGroup = "u10";
							else if (event.includes("12 & Under"))                  var ageGroup = "u12";
							else if (event.includes("14 & Under"))                  var ageGroup = "u14";
							else if (event.includes("16 & Under"))                  var ageGroup = "u16";
							else if (event.includes("18 & Under"))                  var ageGroup = "u18";
							else                                                    var ageGroup = null;

							var newEvent = true;

							for (var k = 0; k < events.length; k++) {
								if (events[k].ageGroup == ageGroup) {
									if (format.includes("Single")) {
										events[k].singlesDrawSizeMen   = singlesDrawSizeMen;
										events[k].singlesDrawSizeWomen = singlesDrawSizeWomen;
									} else if (format.includes("Double")) {
										events[k].doublesDrawSizeMen   = doublesDrawSizeMen;
										events[k].doublesDrawSizeWomen = doublesDrawSizeWomen;
									}
									newEvent = false;
									break;
								}
							}
							
							if (newEvent) {
								events.push({
									event:                    event,
									level:                    level,
									ageGroup:                 ageGroup,
									singlesDrawSizeMen:       singlesDrawSizeMen,
									doublesDrawSizeMen:       doublesDrawSizeMen,
									singlesDrawSizeWomen:     singlesDrawSizeWomen,
									doublesDrawSizeWomen:     doublesDrawSizeWomen,
									totalFinancialCommitment: totalFinancialCommitment
								});
							}
						} catch (e) {
							break;
						}
					}

					var city = components[1].children[2].children[2].children[1].children[0].data;
					
					for (var j = 0; j < data.parent().children()[3].children[2].children.length; j++) {
						try {
							if (data.parent().children()[3].children[2].children[j].children[0].children[0].data == "Indoors/Outdoors:") {
								var numberOfCourts    = data.parent().children()[3].children[2].children[j - 1].children[1].children[0].data;
								var courtType         = data.parent().children()[3].children[2].children[j].children[1].children[0].data;
								var courtSurfaceMen   = data.parent().children()[3].children[2].children[j + 1].children[1].children[0].data;
								var courtSurfaceWomen = data.parent().children()[3].children[2].children[j + 2].children[1].children[0].data;
								if (courtSurfaceMen   == "En-Tou-Cas") courtSurfaceMen   = "Synthetic Grass";
								if (courtSurfaceWomen == "En-Tou-Cas") courtSurfaceWomen = "Synthetic Grass";
								break;
							}
						} catch (e) {
							continue;
						}
					}

					for (var j = 0; j < events.length; j++) {
						//Common Gender

						var tournamentName = tournamentGroups[i][0];

						if (events[j].event.includes("Australian Money Tournament")) {
							var tour      = "Australian Money Tournament";
							var tourShort = "AMT";
						} else if (events[j].event.includes("& Under")) {
							var tour      = "Australian Junior Tournament";
							var tourShort = "AJT";
						} else {
							var tour      = null;
							var tourShort = null;
						}

						if      (events[j].event.includes("Australian Money Tournament")) var ageGroup = "open";
						else if (events[j].event.includes("10 & Under"))                  var ageGroup = "u10";
						else if (events[j].event.includes("12 & Under"))                  var ageGroup = "u12";
						else if (events[j].event.includes("14 & Under"))                  var ageGroup = "u14";
						else if (events[j].event.includes("16 & Under"))                  var ageGroup = "u16";
						else if (events[j].event.includes("18 & Under"))                  var ageGroup = "u18";
						else                                                              var ageGroup = null;

						var level           = events[j].level;
						var latitude        = tournamentGroups[i][5];
						var longitude       = tournamentGroups[i][6];
						var organisationURL = "https://tournaments.tennis.com.au/" + tournamentGroups[i][1];
						var tournamentURL   = null;
						var startDate       = tournamentGroups[i][2];
						var endDate         = tournamentGroups[i][3];
						var address         = tournamentGroups[i][4];
						var country         = "Australia";

						//Men

						if (events[j].singlesDrawSizeMen != null && events[j].doublesDrawSizeMen != null) {
							var tournament = [];

							var gender                   = "male";
							var courtSurface             = courtSurfaceMen;
							var singlesDrawSize          = events[j].singlesDrawSizeMen;
							var doublesDrawSize          = events[j].doublesDrawSizeMen;
							var totalFinancialCommitment = events[j].totalFinancialCommitment;

							tournament.push(tournamentName);
							tournament.push(tour);
							tournament.push(tourShort);
							tournament.push(ageGroup);
							tournament.push(level);
							tournament.push(gender);
							tournament.push(latitude);
							tournament.push(longitude);
							tournament.push(organisationURL);
							tournament.push(tournamentURL);
							tournament.push(startDate);
							tournament.push(endDate);
							tournament.push(courtType);
							tournament.push(courtSurface);
							tournament.push(address);
							tournament.push(country);
							tournament.push(city);
							tournament.push(singlesDrawSize);
							tournament.push(doublesDrawSize);
							tournament.push(totalFinancialCommitment);

							tournaments.push(tournament);
						}

						//Women

						if (events[j].singlesDrawSizeWomen != null && events[j].doublesDrawSizeWomen != null) {
							var tournament = [];

							var gender                   = "female";
							var courtSurface             = courtSurfaceWomen;
							var singlesDrawSize          = events[j].singlesDrawSizeWomen;
							var doublesDrawSize          = events[j].doublesDrawSizeWomen;
							var totalFinancialCommitment = events[j].totalFinancialCommitment;

							tournament.push(tournamentName);
							tournament.push(tour);
							tournament.push(tourShort);
							tournament.push(ageGroup);
							tournament.push(level);
							tournament.push(gender);
							tournament.push(latitude);
							tournament.push(longitude);
							tournament.push(organisationURL);
							tournament.push(tournamentURL);
							tournament.push(startDate);
							tournament.push(endDate);
							tournament.push(courtType);
							tournament.push(courtSurface);
							tournament.push(address);
							tournament.push(country);
							tournament.push(city);
							tournament.push(singlesDrawSize);
							tournament.push(doublesDrawSize);
							tournament.push(totalFinancialCommitment);

							tournaments.push(tournament);
						}
					}

					console.log("\n");

					i++;
					getFactsheetData();
				} catch (e) {
					otherTournaments.push(tournamentGroups[i]);
					i++;
					getFactsheetData();
				}
			}
		}
	}

	function exportOtherTournaments() {
		console.log(otherTournaments);
		fs.writeFile("./australianOtherTournaments.txt", JSON.stringify(otherTournaments), insertTournaments); 
	}

	function insertTournaments(err) {
		if (err) console.log(err);
		console.log(tournaments);
		fs.writeFile("./data/allAustralianTournaments.txt", JSON.stringify(tournaments), finish); 
	}

	function finish(err) {
		if (err) console.log(err);
		console.log("finished");
	}
}