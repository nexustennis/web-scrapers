var http    = require('http');
var cheerio = require('cheerio');
var conn    = require("./database");

getATPtournaments();

function getATPtournaments() {

	request("http://www.atpworldtour.com/en/tournaments", processHTML);

	function processHTML(err, body) {
		$ = cheerio.load(body);
		$('#contentAccordionWrapper').filter(extractData);

		function extractData() {
			var data = $(this);
			var months = data.children();
			var tournaments = [];

			for (var i = 1; i < 13; i++) {
				var monthTournaments = months[i].children[3].children[1].children[1].children;
				for (var j = 0; j < monthTournaments.length; j += 2) {
					var tournament  = [];
					var tempLevel   = monthTournaments[j].children[1].children[1].attribs.src.split("/")[5];
					var tempCountry = monthTournaments[j].children[3].children[3].children[0].data.replace(/(\n|\t)/gm, "").split(", ")[1];
					if (tempCountry.length == 2) tempCountry = "United States of America";
					tempTournamentURL = monthTournaments[j].children[7].children[3].attribs.href.split("//");
					if (tempTournamentURL[1] == "http:") tempTournamentURL = "http://" + tempTournamentURL[2];
					else tempTournamentURL = "http://" + tempTournamentURL[1];

					tournament.push(monthTournaments[j].children[3].children[1].children[0].data); // tournamentName
					tournament.push("male");
					tournament.push("ATP"); // tour
					tournament.push(tempLevel.substr(15, tempLevel.length - 1).split(".")[0]); //level
					tournament.push(null); // latitude
					tournament.push(null); // longitude
					tournament.push("http://www.atpworldtour.com" + monthTournaments[j].children[3].children[1].attribs.href); // organisationURL
					tournament.push(tempTournamentURL); // tournamentURL
					tournament.push(monthTournaments[j].children[3].children[5].children[0].data.replace(/(\n|\t)/gm, "").split(" - ")[0]); // startDate
					tournament.push(monthTournaments[j].children[3].children[5].children[0].data.replace(/(\n|\t)/gm, "").split(" - ")[1]); // endDate
					tournament.push(monthTournaments[j].children[5].children[1].children[1].children[1].children[3].children[1].children[1].children[0].data.replace(/(\n|\t)/gm, "")); // courtType
					tournament.push(monthTournaments[j].children[5].children[1].children[1].children[1].children[3].children[1].children[1].children[3].children[0].data.replace(/(\n|\t)/gm, "")); // courtSurface
					tournament.push(tempCountry); // country
					tournament.push(monthTournaments[j].children[3].children[3].children[0].data.replace(/(\n|\t)/gm, "").split(", ")[0]); // city
					tournament.push(monthTournaments[j].children[5].children[1].children[1].children[1].children[1].children[1].children[1].children[1].children[1].children[0].data.replace(/(\n|\t)/gm, "")); // singlesDrawSize
					tournament.push(monthTournaments[j].children[5].children[1].children[1].children[1].children[1].children[1].children[1].children[3].children[1].children[0].data.replace(/(\n|\t)/gm, "")); // doublesDrawSize
					tournament.push(monthTournaments[j].children[5].children[1].children[1].children[1].children[5].children[1].children[1].children[1].children[0].data.replace(/(\n|\t| )/gm, "")); // totalFinancialCommitment
					
					tournaments.push(tournament);
				}
			}

			var i = 0;

			addCoordinates();

			function addCoordinates() {
				if (i == tournaments.length) printTournaments();
				else {
					console.log("Getting coordinates for " + tournaments[i][0]);
					request(tournaments[i][6], function(err, body) {
						$ = cheerio.load(body);
						$('.tournament-hero-links').filter(getCoordinates);
						function getCoordinates() {
							var data = $(this);
							var url = data[0].children[3].attribs.href;
							var coordinates = url.substr(34, url.length - 1).split("/")[0].split(",");
							if (coordinates[0] != "") tournaments[i][4] = coordinates[0];
							else tournaments[i][4] = null;
							if (coordinates[1] != "") tournaments[i][5] = coordinates[1];
							else tournaments[i][5] = null;
							console.log(tournaments[i]);
							i++;
							addCoordinates();
						}
					});
				}
			}

			function printTournaments() {
				var query = "INSERT INTO tournaments (tournamentName, gender, tour, level, latitude, longitude, organisationURL, tournamentURL, startDate, endDate, courtType, courtSurface, country, city, singlesDrawSize, doublesDrawSize, totalFinancialCommitment) VALUES ?";
		        conn.query(query, [tournaments], finish);
			}

			function finish(err, rows, fields) {
				if (err) console.log(err);
				console.log("finished");
			}
		}
	}
}

function request(url, callback) {
	http.get(url, function(res) {
		var body = '';

	    res.on('data', function(chunk) {
	        body += chunk;
	    });

	    res.on('end', function() {
	        callback(null, body);
	    });
	}).on("error", function(err) {
		callback(err, null);
	});
}