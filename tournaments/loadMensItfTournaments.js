var conn = require("../database");
var fs   = require("fs");

fs.readFile("itfTournaments.txt", function(err, data) {
	tournaments = JSON.parse(data);
	console.log(tournaments);
	
	var query = "INSERT INTO tournaments (tournamentName, tour, tourShort, ageGroup, level, gender, latitude, longitude, organisationURL, tournamentURL, startDate, endDate, courtType, courtSurface, address, country, city, singlesDrawSize, doublesDrawSize, totalFinancialCommitment) VALUES ?";
	conn.query(query, [tournaments], function(err) {
		if (err) console.log(err);
		console.log("done");
	});
});