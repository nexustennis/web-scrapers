var request = require("../functions/request");
var cheerio = require('cheerio');
var conn    = require("../database");
var fs      = require("fs");

Date.prototype.getDateString = function() {
	var day   = this.getDate();
	var month = this.getMonth() + 1;
	var year  = this.getFullYear();

	return year + "-" + month + "-" + day;
}

var tournaments = [];

getATPtournaments();

function getATPtournaments() {
	var i;

	fs.readFile('./data/itfWorldTourMens.html', processHTML);

	function processHTML(err, body) {
		$ = cheerio.load(body);
		$('.sortTab').filter(extractData);
	}

	function extractData() {
		var data = $(this);
		
		var tournamentsHMTL = data.children()[0].children;
		//console.log(tournamentsHMTL[2].children[9].children);
		

		for (i = 2; i < tournamentsHMTL.length; i += 2) {
			var tournament  = [];

			// get dates

			var dateString = tournamentsHMTL[i].children[7].children[0].data.split("\n")[1].split(" ");
			var startDay   = dateString[0];
			var startMonth = dateString[1];
			var endDay     = dateString[3];
			var endMonth   = dateString[4];
			var year       = dateString[5];

			var startDate = new Date(startMonth + " " + startDay + " " + year);
			var endDate   = new Date(endMonth   + " " + endDay   + " " + year);			

			//official tournament data

			var tournamentName  = tournamentsHMTL[i].children[1].children[1].children[1].children[0].data;
			var tour            = "ITF World Tennis Tour";
			var tourShort       = "ITF";
			var ageGroup        = "open";
			var level           = tournamentName.split(" ")[0];
			var gender          = "male";
			var latitude;
			var longitude;
			var organisationURL = "www.itftennis.com" + tournamentsHMTL[i].children[1].children[1].children[1].attribs.href;
			var tournamentURL   = null;
			var startDate       = (new Date(startMonth + " " + startDay + " " + year)).getDateString();
			var endDate         = (new Date(endMonth   + " " + endDay   + " " + year)).getDateString();
			var courtType       = "Unknown";
			var courtSurface    = "Unknown Surface";
			var address;
			var country         = tournamentsHMTL[i].children[5].children[1].children[2].data;
			var city            = tournamentsHMTL[i].children[3].children[0].data.split("\n")[1];
			var singlesDrawSize = "32";
			var doublesDrawSize = "16";
			var totalFinancialCommitment = tournamentsHMTL[i].children[9].children[0].data.split("\n")[1];

			//fs.appendFileSync("./data/itfTournaments/urls.txt", "https://" + organisationURL + "\n");

			tournament.push(tournamentName);
			tournament.push(tour);
			tournament.push(tourShort);
			tournament.push(ageGroup);
			tournament.push(level);
			tournament.push(gender);
			tournament.push(latitude);
			tournament.push(longitude);
			tournament.push(organisationURL);
			tournament.push(tournamentURL);
			tournament.push(startDate);
			tournament.push(endDate);
			tournament.push(courtType);
			tournament.push(courtSurface);
			tournament.push(address);
			tournament.push(country);
			tournament.push(city);
			tournament.push(singlesDrawSize);
			tournament.push(doublesDrawSize);
			tournament.push(totalFinancialCommitment);
				
			tournaments.push(tournament);
		}
		
		//console.log(tournaments);

		i = 0;
		getLocationDetails();
	}

	function getLocationDetails() {
		if (i == tournaments.length) {
			i = 0;
			//console.log(tournaments);
			fs.writeFile("itfTournaments.txt", JSON.stringify(tournaments), function(err) {
				if (err) console.log(err);
				console.log("done");
			});
			return;
		}

		var url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBL-SId6Ibn2HTOgMsihSbD-47Fk4GmziI&address=" + tournaments[i][16] + ", " + tournaments[i][15];
		console.log(url);

		request(url, getCoordinates);

		function getCoordinates(err, data) {
			data = JSON.parse(data);
			try {
				var coordinates = data.results[0].geometry.location;

				var country;

				for (var j = 0; j < data.results[0].address_components.length; j++) {
					if (data.results[0].address_components[j].types[0] == "country") country = data.results[0].address_components[j].long_name;
				}

				var address     = tournaments[i][16] + ", " + country;

				var latitude    = coordinates.lat;
				var longitude   = coordinates.lng;
			} catch(e) {
				var address   = null;
				var country   = null;
				var latitude  = null;
				var longitude = null;
			}

			tournaments[i][14] = address;
			tournaments[i][15] = country;
			tournaments[i][6] = latitude;
			tournaments[i][7] = longitude;

			console.log(tournaments[i][0]);

			i++;
			getLocationDetails();
		}
	}

	

	function getTournamentPageData() {
		for (var i = 0; i < 1/*tournaments.length*/; i++) {
			console.log("https://" + tournaments[i][8]);
			request("https://" + tournaments[i][8], processTournamentsHTML);
			function processTournamentsHTML(err, tournamentBody) {
				//var res = _eval()
				//$ = cheerio.load(tournamentBody);
				console.log(tournamentBody);
				//var url = "https://itftennis.com" + tournamentBody.substring(tournamentBody.indexOf("<iframe") + 13, tournamentBody.indexOf("</iframe>")).split("\"")[0];
				var url = "https://itftennis.com" + tournamentBody.substring(tournamentBody.indexOf("<iframe") + 13, tournamentBody.indexOf("</iframe>")).split("\"")[0];
				console.log(url);
				request(url, getData);
				function getData(err, newHtml) {
					console.log(newHtml);
				}
				/*$('.pn').filter(extractTournamentData);
				console.log(tournamentBody);
				function extractTournamentData() {
					console.log("Sorted Data");
					var data = $(this);
					console.log(data);
				}*/
			}
		}
	}

	function exportTournaments() {
		console.log(tournaments);

		fs.writeFile("./itfTournaments.txt", JSON.stringify(tournaments), finish); 
	}

	function printTournaments() {
		var query = "INSERT INTO tournaments (tournamentName, gender, tour, level, latitude, longitude, organisationURL, tournamentURL, startDate, endDate, courtType, courtSurface, country, city, singlesDrawSize, doublesDrawSize, totalFinancialCommitment) VALUES ?";
        conn.query(query, [tournaments], finish);
	}

	function finish(err, rows, fields) {
		if (err) console.log(err);
		console.log("finished");
	}
}