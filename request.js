var http    = require('http');

function request(url, callback) {
	http.get(url, function(res) {
		var body = '';

	    res.on('data', function(chunk) {
	        body += chunk;
	    });

	    res.on('end', function() {
	        callback(null, body);
	    });
	}).on("error", function(err) {
		callback(err, null);
	});
}

module.exports = request;