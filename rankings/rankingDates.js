var getRankingDates = require("../functions/getRankingDates");

function updateATPrankingDates() {
	var existingDates = [];

	getRankingDates("atp", "singles", getRankingDatesResult);

	function getRankingDatesResult(dates) {
		existingDates = dates;
		request("http://www.atpworldtour.com/en/rankings/singles", processHTML);
	}

	function processHTML(err, body) {
		$ = cheerio.load(body);
		$('#filterHolder').filter(extractData);

		function extractData() {
			var data = $(this);
			var weeks = data.children()[0].children[1].children[1].children[1].children[3].children;
			console.log(weeks[3].attribs["data-value"]);
			
			var dates = [];

			for (var i = 3; i < weeks.length; i += 2) {
				var dateString = weeks[i].attribs["data-value"];
				if (!existingDates.includes(dateString)) {
					var date = [];

					date.push("atp");
					date.push("singles");
					date.push(date);

					dates.push(date);
				}
			}

			insertDates();

			function insertDates() {
				var query = "INSERT INTO rankingDates (organisation, category, date) VALUES ?";
		        conn.query(query, [dates], finish);
			}

			function finish(err, rows, fields) {
				if (err) console.log(err);
				console.log("finished");
			}
			
		}
	}
}