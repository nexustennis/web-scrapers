var request          = require("../request");
var cheerio          = require("cheerio");
var conn             = require("../database");
var getRankingDates  = require("../functions/getRankingDates");
var getAllATPplayers = require("../functions/getAllATPplayers");
var rankingDates     = require("../functions/rankingDates");

getATPrankings();

function getATPrankings() {
	var playerList = [];

	rankingDates.getNewATPsinglesRankingDates(rankingDatesResult);

	function rankingDatesResult(dates) {
		dates = dates.slice(dates.length - 3, dates.length - 1);
		var i = 0;

		rankingDates.insertRankingDates("atp", "singles", dates, loopThroughDates);

		function loopThroughDates() {
			if (i >= dates.length) insertPlayers();
			else {

				console.log("started " + dates[i][2]);
				getATPrankingsForDate(dates[i][3], dates[i][2], loopThroughDates);
				i++;
			}
		}
	}

	function getATPrankingsForDate(rankingDateID, date, callback) {
		var url = "http://www.atpworldtour.com/en/rankings/singles?rankDate=" + date + "&rankRange=1-5000";
		request(url, processHTML);

		function processHTML(err, body) {
			$ = cheerio.load(body);
			$('#singlesRanking').filter(extractData);
		}

		function extractData() {
			var data = $(this);
			var players = data.children()[0].children[1].children[3].children;
			
			var rankings = [];

			for (var i = 1; i < players.length; i += 2) {
				var ranking  = [];
				var player = [];

				var moveDirection = players[i].children[3].children[1].attribs.class;
				var moveAmount = players[i].children[3].children[3].children[0].data.replace(/(\n|\t)/gm, "");
				if (moveDirection == "move-none") moveAmount = 0;
				else if (moveDirection == "move-down") moveAmount = moveAmount * -1;

				var organisation = "atp";
				var playerName = players[i].children[7].children[1].children[0].data;
				var profileURL = "www.atpworldtour.com" + players[i].children[7].children[1].attribs.href;

				ranking.push(rankingDateID); // date
				ranking.push(organisation); // organisation
				ranking.push("singles"); // format
				ranking.push(players[i].children[1].children[0].data.replace(/(\n|\t|T)/gm, "")); // rank
				ranking.push(playerName); // playerName
				ranking.push(profileURL); // profileURL
				ranking.push(players[i].children[11].children[1].children[0].data.replace(/(,)/gm, "")); // points
				ranking.push(moveAmount); // move
				ranking.push(players[i].children[15].children[0].data.replace(/(\n|\t|,)/gm, "")); // pointsDropping
				ranking.push(players[i].children[13].children[1].children[0].data); // tournamentsPlayed

				rankings.push(ranking);

				if (!playerExists(profileURL)) {
					player.push(organisation);
					player.push(playerName);
					player.push(profileURL);

					playerList.push(player);
				}
			}

			insertRankings();

			function insertRankings() {
				var query = "INSERT INTO rankings (rankingDateID, organisation, category, rank, playerName, profileURL, points, move, pointsDropping, tournamentsPlayed) VALUES ?";
		        conn.query(query, [rankings], finish);
			}

			function finish(err, rows, fields) {
				if (err) console.log(err);
				console.log("finished " + date);
				callback();
			}
		}
	}

	function playerExists(profileURL) {
		for (var i = 0; i < playerList.length; i++) 
			if (playerList[i].includes(profileURL)) return true;

		return false;
	}

	function insertPlayers() {
		var query = "INSERT INTO players (organisation, playerName, profileURL) VALUES ?";
	    conn.query(query, [playerList], updatePlayerID);
	}

	function updatePlayerID() {
		var query = "UPDATE rankings SET rankings.playerID = (SELECT players.playerID FROM players WHERE players.profileURL = rankings.profileURL)";
	    conn.query(query, finish);	
	}

	function finish(err) {
		if (err) throw err;
		console.log("finised getting rankings");
	}
	
}