var getAllATPplayers = require("../functions/getAllATPplayers");
var conn = require("../database");
var request         = require("../request");
var cheerio         = require("cheerio");
var getRankingDates = require("../functions/getRankingDates");

players = [];

getATPplayers();

function getATPplayers() {
	getRankingDates("atp", "singles", rankingDatesResult);

	function rankingDatesResult(dates) {
		console.log(dates);
		var i = 0;

		loopThroughDates();

		function loopThroughDates() {
			if (i > 10/*dates.length*/) finish();
			else {
				var day = dates[i].date.getDate();
				var month = dates[i].date.getMonth() + 1;
				var year = dates[i].date.getFullYear();
				var date = year + "-" + month + "-" + day;
				console.log("started " + date);
				getATPplayersForDate(dates[i].rankingDateID, date, loopThroughDates);
				i++;
			}
		}

		insertPlayers();

		function insertPlayers() {
			var query = "INSERT INTO players (organisation, playerName, profileURL) VALUES ?";
	        conn.query(query, [players], finish);
		}

		function finish(err, rows, fields) {
			if (err) console.log(err);
			console.log("finished getting players");
		}
	}
	
}

function getATPplayersForDate(rankingDateID, date, callback) {
	var url = "http://www.atpworldtour.com/en/rankings/singles?rankDate=" + date + "&rankRange=1-5000";
	request(url, processHTML);

	function processHTML(err, body) {
		$ = cheerio.load(body);
		$('#singlesRanking').filter(extractData);

		function extractData() {
			var data = $(this);
			var rankings = data.children()[0].children[1].children[3].children;
			
			for (var i = 1; i < rankings.length; i += 2) {
				var player  = [];

				player.push("atp"); // organisation
				player.push("singles"); // format
				player.push(rankings[i].children[7].children[1].children[0].data); // playerName
				player.push("www.atpworldtour.com" + rankings[i].children[7].children[1].attribs.href); // profileURL

				players.push(player);
			}
		}

		callback();
	}
}

function playerExists(player) {

}






function getATPplayers() {
	getAllATPplayers(getAllATPplayersResult);

	function getAllATPplayersResult(atpPlayers) {
		//console.log(atpPlayers);

		var players = [];
		var temp;

		for (var i = 0; i < atpPlayers.length; i++) {
			temp = [];

			temp.push("atp");
			temp.push(atpPlayers[i].playerName);
			temp.push(atpPlayers[i].profileURL);

			players.push(temp);
		}

		console.log(players);

		var query = "INSERT INTO players (organisation, playerName, profileURL) VALUES ?";
		conn.query(query, [players], finish);
	}

	function finish() {
		console.log("finished");
	}
}